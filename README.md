<h1 align="center">Mileapp Test - Backend</h1>

## Feature

- Create Package 
- Store Package 
- Update Package 
- Patch Package 
- Delete Package 
- Get Package 
- Get All Package 
- MongoDB
- Swagger Documentation  
- Testing  
- Docker  


## How to run

There are two ways to run this application, with docker or without docker

```bash
# running with docker

# copy .env
cp .env.example .env

# running in development mode
docker-compose up -build
```

## How to stop

```bash
# remove container
docker-compose down
```


## Run tests

```bash
php artisan test
```
## Application 

- Swagger Documentation run in http://localhost/api/documentation
- API Prefix run in http://localhost/api/v1/
- Greetings run in http://localhost
- Monggo Express run in http://localhost:8081/
