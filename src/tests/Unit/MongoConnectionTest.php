<?php

namespace Tests\Unit;

use MongoDB\Client;

use PHPUnit\Framework\TestCase;
use config;

class MongoConnectionTest extends TestCase
{
    public function testMongoConnection()
    {
        $mongo = new Client(
            config('database.connections.mongodb.host'),
            [
                'username' => config('database.connections.mongodb.username'),
                'password' => config('database.connections.mongodb.password'),
                'db' => config('database.connections.mongodb.database')
            ]
        );

        $this->assertNotNull($mongo);
    }
}
