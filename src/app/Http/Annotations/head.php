<?php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Mileapp test",
 *      description="Mileapp test",
 *      @OA\Contact(
 *          email="muhammad.charis.az@gmail.com"
 *      ),
 * )
 */

/**
 *  @OA\Server(
 *      url="/api/v1",
 *      description="Mileapp package api"
 *  )
 *
 */
